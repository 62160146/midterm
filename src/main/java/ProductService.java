
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ii drunkboy
 */
public class ProductService {

    private static ArrayList<Product> productList = new ArrayList<>();
    private static double totalprice;
    private static int total;

//    static {
//        //Mock Data
//        productList.add(new Product("001", "SOLO", "YG", 650.00, 5));
//        productList.add(new Product("002", "-R-", "YG", 690.00, 10));
//    }
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }

    public static boolean editProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }

    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }

    public static boolean clearProduct(int index) {
        productList.clear();
        save();
        return true;
    }

    public static ArrayList<Product> getProducts() {
        return productList;
    }

    public static Product getProduct(int index) {
        return productList.get(index);
    }

    public static Double TotalPrice(ArrayList<Product> product) {
        totalprice=0;
        for (int i = 0; i < productList.size(); i++) {
            totalprice += (productList.get(i).getPrice() * productList.get(i).getAmount());
        }
        save();
        return totalprice;
    }
    public static Integer Total(ArrayList<Product> product) {
        total = 0;
        for (int i = 0; i < productList.size(); i++) {
            total += productList.get(i).getAmount();
        }
        save();
        return total;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Akai.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Akai.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
