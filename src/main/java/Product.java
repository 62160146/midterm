
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ii drunkboy
 */
public class Product implements Serializable{
    private String ID;
    private String Name;
    private String Brand;
    private double Price;
    private int Amount;

    public Product(String ID, String Name, String Brand, double Price, int Amount) {
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "        "+ID + "                            " + Name +
                "                          " + Brand + 
                "                               " + Price +
                "                                  " + Amount;
    }

}
